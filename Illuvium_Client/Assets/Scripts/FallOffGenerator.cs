﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class FalloffGenerator
{

    public static float[,] GenerateSquareFalloffMap(int _size, float _radius)
	{
        float[,] map = new float[_size, _size];

        for(int i = 0; i < _size; i++) {
            for(int j = 0; j < _size; j++) {
                float x = i / (float)_size * 2f - 1f;
                float y = j / (float)_size * 2f - 1f;

                float value = Mathf.Max(Mathf.Abs(x), Mathf.Abs(y));
                map[i, j] = Evaluate(value, _radius);
            }
        }

        return map;
    }

    public static float[,] GenerateRadialFalloffMap(int _size, float _radius)
	{
        float[,] map = new float[_size, _size];

        for(int i = 0; i < _size; i++) {
            for(int j = 0; j < _size; j++) {
                float x = i / (float)_size * 2f - 1f;
                float y = j / (float)_size * 2f - 1f;

                float value = Mathf.Sqrt((x * x) + (y * y));
                map[i, j] = Evaluate(value, _radius);
            }
        }

        return map;
    }

    public static float Evaluate(float _value, float _intensity = 5f, float _offset = 1f)
	{
		float x = Mathf.Pow(_value, _intensity);
		float x_offset = Mathf.Pow(_offset - _value, _intensity);

		return x / (x + x_offset);
    }
}
