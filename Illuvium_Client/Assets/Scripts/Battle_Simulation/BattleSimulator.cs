using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleSimulator : MonoBehaviour
{
	[SerializeField] BattleGrid grid;
	[SerializeField] BattleGridSettings settings;

	[SerializeField] Unit unitPrefab;

	[SerializeField] List<TeamInfo> competingTeams = new List<TeamInfo>();
	[SerializeField] TeamInfo defaultTeamInfo;

	[SerializeField] List<Unit> unitInstances = new List<Unit>();

	BattleSnapshot currentSnapshot;

	public void InitializeSim(BattleGridSettings _settings, List<TeamInfo> _teamOverride)
	{
		grid.InitializeBattleGrid(_settings);
		competingTeams = _teamOverride;
	}

	void InitializeTeams()
	{
		for(int i = 0; i < competingTeams.Count; i++) {
			TeamInfo team = competingTeams[i];

			SpawnTeamStartingUnits(team);
		}
	}

	void ClearBoard()
	{
		for(int i = unitInstances.Count - 1; i >= 0; i--) {
			unitInstances[i].ReturnToPool();
		}
	}

	void SpawnTeamStartingUnits(TeamInfo _team)
	{
		int unitsLeft = _team.startingUnits;

		while(unitsLeft > 0) {
			Unit newUnit = SpawnUnit(_team.id);

			GridCell randomCell = grid.GetRandomCell();
			if(randomCell != null) {
				newUnit.transform.position = randomCell.WorldPos;
			}

			unitsLeft--;
		}
	}

	TeamInfo FindTeamInfo(int _teamID)
	{
		TeamInfo teamInfo;
		System.Predicate<TeamInfo> matchingID = delegate (TeamInfo _t) { return _t.id == _teamID; };
		if(competingTeams.Exists(matchingID)) {
			teamInfo = competingTeams.Find(matchingID);
		}
		else {
			teamInfo = defaultTeamInfo;
		}

		return teamInfo;
	}
	
	Unit SpawnUnit(int _team)
	{
		TeamInfo teamInfo = FindTeamInfo(_team);

		Unit newUnit = Pooling.GetObject(unitPrefab);
		newUnit.AssignColor(teamInfo.color);

		newUnit.onDestroy += (OnUnitDestroyed);

		unitInstances.Add(newUnit);

		return newUnit;
	}

	void OnUnitDestroyed(Unit _destroyedUnit)
	{
		if(unitInstances.Contains(_destroyedUnit)) {
			unitInstances.Remove(_destroyedUnit);
		}

		_destroyedUnit.onDestroy -= OnUnitDestroyed;


	}

	void FullBattleUpdate()
	{
		UnitSnapshot[] unitState = currentSnapshot.unitState;
		
		for(int i = 0; i < unitState.Length; i++) {
			UnitSnapshot unitSnapshot = unitState[i];
			System.Predicate<Unit> matchingID = delegate (Unit _u) { return _u.ID == unitSnapshot.unitArenaID; };

			Unit match = unitInstances.Find(matchingID);
	
			if(match != null) {
				match.LoadSnapshot(unitSnapshot);

				GridCell newCell = grid.GetCell(unitSnapshot.GridCoords);
				if(newCell != null) {
					match.TargetPos = newCell.WorldPos;
				}

				if(unitSnapshot.hasAttacked) {
					match.PlayAttackAnimation();
				}
			}

		}
	}

	public BattleGridSettings GridSettings {
		get {
			return settings;
		}
	}

	public void InitialBattleSetup(BattleSnapshot _snapshot)
	{
		ClearBoard();

		currentSnapshot = _snapshot;

		UnitSnapshot[] unitState = currentSnapshot.unitState;
		for(int i = 0; i < unitState.Length; i++) {
			UnitSnapshot unitSnapshot = unitState[i];

			Unit newUnit = SpawnUnit(unitSnapshot.team);
			newUnit.ID = unitSnapshot.unitArenaID;
			newUnit.LoadSnapshot(unitSnapshot);


			GridCell spawnCell = grid.GetCell(unitSnapshot.GridCoords);
			if(spawnCell != null) {
				newUnit.WorldPos = spawnCell.WorldPos;
				print("Spawning unit at " + unitSnapshot.GridCoords + " World pos = " + spawnCell.WorldPos);

			}


		}
	}

	public void UpdateBattleSnapshot(BattleSnapshot _snapshot)
	{
		currentSnapshot = _snapshot;

		FullBattleUpdate();

	}
}

[System.Serializable]
public struct TeamInfo
{
	public int id;
	public string displayName;
	public Color color;

	public int startingUnits;
}