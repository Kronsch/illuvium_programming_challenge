using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Unit : MonoBehaviour
{
	private new Transform transform;
	private new MeshRenderer renderer;
	private Animator animator;
	private PoolReturner poolReturner;

	[SerializeField] UnitCanvas unitCanvas;
	[SerializeField] ParticleSystem attackParticles;
	[SerializeField] float movementLerpTime = 0.1f;

	int arenaID;

	int hp;
	int maxHP;

	bool isDead;

	Vector3 currentPos;
	Vector3 targetPos;
	Vector3 vel;

	public event UnityAction<Unit> onDestroy;
	public event UnityAction onHPChanged;

	private void Awake()
	{
		transform = base.transform;
		renderer = GetComponentInChildren<MeshRenderer>();
		animator = GetComponentInChildren<Animator>();
		poolReturner = GetComponent<PoolReturner>();

		if(unitCanvas != null) {
			unitCanvas.InitializeUnit(this);
		}
	}

	private void Update()
	{
		currentPos = transform.position;
		currentPos = Vector3.SmoothDamp(currentPos, targetPos, ref vel, movementLerpTime);
		transform.position = currentPos;
	}

	private void UpdateAnimator()
	{
		animator.SetBool("isDead", isDead);
	}

	public int ID {
		get {
			return arenaID;
		}
		set {
			arenaID = value;
		}
	}

	public float NormalizedHealth {
		get {
			if(maxHP > 0) {
				return (hp / (float)maxHP);
			}

			return 0f;
		}
	}

	public Vector3 TargetPos {
		set {
			targetPos = value;
		}
	}

	public Vector3 WorldPos {
		get {
			return transform.position;
		}
		set {
			transform.position = value;
			targetPos = value;
		}
	}
	
	public void LoadSnapshot(UnitSnapshot _snapshot)
	{
		hp = _snapshot.hp;
		maxHP = _snapshot.maxHP;

		isDead = hp <= 0;

		onHPChanged.Invoke();

		UpdateAnimator();
	}

	public void PlayAttackAnimation()
	{
		attackParticles.Play();
	}
	
	public void AssignColor(Color _color)
	{
		if(renderer != null) {
			Material[] m = renderer.materials;
			foreach(Material mat in m) {
				mat.color = _color;
			}
		}

		if(attackParticles != null) {
			ParticleSystem.MainModule main = attackParticles.main;
			main.startColor = _color;
		}
	}

	public void ReturnToPool()
	{
		poolReturner.Return();
	}
}
