using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridCell : MonoBehaviour
{
	[SerializeField] GridCoordinates coords;

	public bool walkable;

	int index;
	public int Index {
		get {
			return index;
		}
		set {
			index = value;
		}
	}

	public Vector3 WorldPos {
		get {
			return transform.position;
		}
	}

	public GridCoordinates GridPos {
		get {
			return coords;
		}
	}

	public void InitializeCell(int _xCoord, int _yCoord)
	{
		coords = new GridCoordinates(_xCoord, _yCoord);
	}

}
