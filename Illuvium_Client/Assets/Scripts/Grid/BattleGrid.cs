using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleGrid : MonoBehaviour
{
	[SerializeField] GridCell cellPrefab;
	[SerializeField] BattleGridSettings settings;

	[SerializeField] float cubeCellHeightSetting = 0.2f; //Only needed for testing the grid with cubes.

	int currentGridWidth;
	int currentGridLength;
	List<GridCell> cells = new List<GridCell>();

	public void InitializeBattleGrid(BattleGridSettings _settingsOverride)
	{
		ClearGrid();
		GenerateGrid(_settingsOverride);
	}

	void GenerateGrid(BattleGridSettings _settings)
	{
		int width, length;

		width = _settings.GridWidth;
		length = _settings.GridLength;

		int cellIndex = 0;
		for(int x = 0; x < width; x++) {
			for(int y = 0; y < length; y++) {
				GridCell newCell = CreateGridCell(x, y);
				newCell.Index = cellIndex++;

				Vector3 worldPos = new Vector3();
				worldPos.x = (x * _settings.GetSizeWithSpacing());
				worldPos.y = transform.position.y;
				worldPos.z = (y * _settings.GetSizeWithSpacing());

				Vector3 scale = new Vector3();
				scale.x = _settings.CellSize;
				scale.y = cubeCellHeightSetting;
				scale.z = _settings.CellSize;

				newCell.transform.position = worldPos;
				newCell.transform.localScale = scale;
			}
		}

		currentGridWidth = width;
		currentGridLength = length;
	}

	void ClearGrid()
	{
		for(int i = cells.Count - 1; i >= 0; i--) {
			Destroy(cells[i]);
		}

		cells.Clear();
	}

	GridCell CreateGridCell(int _xCoord, int _yCoord)
	{
		GridCell newCell = Instantiate(cellPrefab);

		newCell.InitializeCell(_xCoord, _yCoord);
		newCell.transform.SetParent(transform);

		cells.Add(newCell);

		return newCell;
	}

	public GridCell GetCell(GridCoordinates _coords)
	{

		int y = _coords.Y;
		if(y < 0 || y >= currentGridLength) {
			return null;
		}

		int x = _coords.X;
		if(x < 0 || x >= currentGridWidth) {
			return null;
		}

		return cells[(x * currentGridWidth) + y];
	}

	public GridCell GetRandomCell()
	{
		return cells[Random.Range(0, cells.Count)];
	}
}

[System.Serializable]
public class BattleGridSettings
{
	public int GridWidth = 64;
	public int GridLength = 64;

	public float CellSize = 2f;
	public float CellSpacing = 0.5f;

	public float GetSizeWithSpacing()
	{
		return CellSize + CellSpacing;
	}
}