using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct GridCoordinates
{
	[SerializeField]
	private int x, y;

	public int X {
		get {
			return x;
		}
	}
	public int Y {
		get {
			return y;
		}
	}

	public GridCoordinates(int _x, int _y)
	{
		x = _x;
		y = _y;
	}

	public GridCoordinates(Vector2Int _vector)
	{
		x = _vector.x;
		y = _vector.y;
	}

	public static GridCoordinates operator +(GridCoordinates _lhs, GridCoordinates _rhs)
	{
		return new GridCoordinates(_lhs.x + _rhs.x, _lhs.y + _rhs.y);
	}

	public static GridCoordinates operator -(GridCoordinates _lhs, GridCoordinates _rhs)
	{
		return new GridCoordinates(_lhs.x - _rhs.x, _lhs.y - _rhs.y);
	}

	public static bool operator ==(GridCoordinates _lhs, GridCoordinates _rhs)
	{
		return (_lhs.x == _rhs.x) && (_lhs.y == _rhs.y);
	}

	public static bool operator !=(GridCoordinates _lhs, GridCoordinates _rhs)
	{
		return (_lhs.x != _rhs.x) || (_lhs.y != _rhs.y);
	}

	public static GridCoordinates Zero {
		get {
			return new GridCoordinates(0, 0);
		}
	}

	public static GridCoordinates MinValue {
		get {
			return new GridCoordinates(int.MinValue, int.MinValue);
		}
	}

	public static int TileDistanceCalc(int _xDist, int _yDist)
	{

		if(_xDist > _yDist)
			return 14 * _yDist + 10 * (_xDist - _yDist);
		return 14 * _xDist + 10 * (_yDist - _xDist);
	}

	public int DistanceTo(GridCoordinates _other)
	{
		int xDist = Mathf.Abs(x - _other.x);
		int yDist = Mathf.Abs(y - _other.y);

		return GridCoordinates.TileDistanceCalc(xDist, yDist);
	}

	public GridCoordinates[] GetNeighbors() {
		GridCoordinates[] neighbors = new GridCoordinates[8];

		neighbors[0] = new GridCoordinates(x - 1, y - 1);
		neighbors[1] = new GridCoordinates(x - 1, y);
		neighbors[2] = new GridCoordinates(x - 1, y + 1);
		neighbors[3] = new GridCoordinates(x, y + 1);
		neighbors[4] = new GridCoordinates(x, y - 1);
		neighbors[5] = new GridCoordinates(x + 1, y - 1);
		neighbors[6] = new GridCoordinates(x + 1, y);
		neighbors[7] = new GridCoordinates(x + 1, y + 1);

		return neighbors;
	}

	public override string ToString()
	{
		return "(" + X.ToString() + ", " + Y.ToString() + ")";
	}

	public override bool Equals(object obj)
	{
		if(!(obj is GridCoordinates)) {
			return false;
		}

		var coordinates = (GridCoordinates)obj;
		return x == coordinates.x &&
			   y == coordinates.y;
	}

	public override int GetHashCode()
	{
		var hashCode = 1553271884;
		hashCode = hashCode * -1521134295 + x.GetHashCode();
		hashCode = hashCode * -1521134295 + y.GetHashCode();
		return hashCode;
	}
}
