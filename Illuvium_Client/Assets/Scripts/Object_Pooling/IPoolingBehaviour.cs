﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPoolingBehaviour 
{
	void OnPoolingSetActive(bool _active);

}
