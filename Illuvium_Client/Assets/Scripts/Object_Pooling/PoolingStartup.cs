using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolingStartup : MonoBehaviour
{
	[SerializeField] PoolingDataCenter dataCenter;

	private void Awake()
	{
		Pooling.EstablishDataCenter(dataCenter);
	}

}
