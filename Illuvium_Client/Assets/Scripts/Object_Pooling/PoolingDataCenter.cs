﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu(menuName = "Pooling Data Center")]
public class PoolingDataCenter : ScriptableObject
{
	List<Pool> pools = new List<Pool>();


	private void Awake()
	{
		SceneManager.activeSceneChanged += OnSceneChanged;
	}

	public void InitializeObjectPool()
	{
		pools = new List<Pool>();
		Pooling.EstablishDataCenter(this);
	}

	public Pool FindMatchingPool<K>(K _source)
	{
		return pools.Find(Pool.MatchingSource(_source));
	}

	public Pool CreateNewPool(GameObject _sourceObject, int _startingAmount = 5) 
	{
		System.Predicate<Pool> match = Pool.MatchingSource(_sourceObject);
		if(pools.Exists(match)) {
			return pools.Find(match);
		}

		Pool newPool = new Pool(_sourceObject, _startingAmount);
		pools.Add(newPool);
		return newPool;
	}

	public void ClearPools()
	{
		foreach(Pool p in pools) {
			p.ClearPool(true);
		}
		pools.Clear();
	}

	void OnSceneChanged(Scene _a, Scene _b)
	{
		ClearPools();
	}


	public void PrintoutToConsole()
	{
		Debug.Log("Pooling Data Center printout:");
		foreach(Pool p in pools) {
			Debug.Log("Pool " + p.Source + " with " + p.Size + " instances");
		}
		Debug.Log("Pooling Data Center printout (end)");
	}
}
