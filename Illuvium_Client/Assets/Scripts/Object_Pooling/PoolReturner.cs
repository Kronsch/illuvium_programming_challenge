﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC;

public class PoolReturner : MonoBehaviour
{
	Pool poolRef;

	CoroutineHandle currentReturnCoroutine;

	public void SetPoolReference(Pool _pool)
	{
		poolRef = _pool;
	}

	public void ClearPoolReference()
	{
		poolRef = null;
	}

	public void Return()
	{
		if(poolRef == null) {
			Debug.LogWarning(new System.Exception("Pool Return failed. Pool Reference not set."));
			Destroy(gameObject);
			return;
		}

		poolRef.ReturnObject(gameObject);
	}

	public void ReturnAfter(float _delay)
	{
		if(currentReturnCoroutine != null) {
			Timing.KillCoroutines(currentReturnCoroutine);
		}

		currentReturnCoroutine = Timing.RunCoroutine(WaitRoutine(_delay));
	}

	IEnumerator<float> WaitRoutine(float _duration)
	{
		yield return Timing.WaitForSeconds(_duration);
		Return();
	}
}
