﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Pooling
{
	public static PoolingDataCenter data;

	static bool Active {
		get {
			return data != null;
		}
	}

	public static bool DataCenterEstablished {
		get {
			return Active;
		}
	}

	public static void EstablishDataCenter(PoolingDataCenter _data)
	{
		data = _data;
	}

	public static void FillPool<K>(K _source, int _objectCount)
	{
		if(Active) {
			if(_source is Component == false) {
				Debug.LogException(new System.Exception("Object Pool construction failed. Source object passed in doesn't derive from 'Component'"));
				return;
			}

			Pool targetPool = data.FindMatchingPool(_source);

			if(targetPool == null) {
				GameObject sourceObject = (_source as Component).gameObject;
				targetPool = data.CreateNewPool(sourceObject, _objectCount);
			}
			else {
				targetPool.FillPool(_objectCount);
			}
		}
		else {
			Debug.LogException(new System.Exception("Object Pool construction failed. No Pooling Data Center established."));
		}
	}

	public static void FillPool(GameObject _source, int _objectCount)
	{
		if(Active) {
			Pool targetPool = data.FindMatchingPool(_source);

			if(targetPool == null) {
				targetPool = data.CreateNewPool(_source, _objectCount);
			}
			else {
				targetPool.FillPool(_objectCount);
			}
		}
		else {
			Debug.LogException(new System.Exception("Object Pool construction failed. No Pooling Data Center established."));
		}
	}

	public static K GetObject<K>(K _source, int _startingAmount = 5)
	{
		if(Active) {
			if(_source is Component == false) {
				Debug.LogException(new System.Exception("Object Pool construction failed. Source object passed in doesn't derive from 'Component'"));
				return default(K);
			}

			Pool targetPool = data.FindMatchingPool(_source);

			if(targetPool == null) {
				GameObject sourceObject = (_source as Component).gameObject;
				targetPool = data.CreateNewPool(sourceObject, _startingAmount);
			}

			GameObject newInst = targetPool.GetObject();
			return newInst.GetComponent<K>();
		}
		else {
			Debug.LogException(new System.Exception("Object Pool construction failed. No Pooling Data Center established."));
			return default(K);
		}
	}

	public static GameObject GetObject(GameObject _source, int _startingAmount = 5)
	{
		if(Active) {
			Pool targetPool = data.FindMatchingPool(_source);

			if(targetPool == null) {
				targetPool = data.CreateNewPool(_source, _startingAmount);
			}

			GameObject newInst = targetPool.GetObject();
			return newInst;
		}
		else {
			Debug.LogException(new System.Exception("Object Pool construction failed. No Pooling Data Center established."));
			return null;
		}
	}

	public static K GetObject<K>(K _source, Vector3 _position, Quaternion _rotation, int _startingAmount = 5)
	{
		if(Active) {
			if(_source is Component == false) {
				Debug.LogException(new System.Exception("Object Pool construction failed. Source object passed in doesn't derive from 'Component'"));
				return default(K);
			}

			Pool targetPool = data.FindMatchingPool(_source);

			if(targetPool == null) {
				GameObject sourceObject = (_source as Component).gameObject;
				targetPool = data.CreateNewPool(sourceObject, _startingAmount);
			}

			GameObject newInst = targetPool.GetObject();
			newInst.transform.position = _position;
			newInst.transform.rotation = _rotation;

			return newInst.GetComponent<K>();
		}
		else {
			Debug.LogException(new System.Exception("Object Pool construction failed. No Pooling Data Center established."));
			return default(K);
		}
	}

	public static GameObject GetObject(GameObject _source, Vector3 _position, Quaternion _rotation, int _startingAmount = 5)
	{
		if(Active) {
			Pool targetPool = data.FindMatchingPool(_source);

			if(targetPool == null) {
				targetPool = data.CreateNewPool(_source, _startingAmount);
			}

			GameObject newInst = targetPool.GetObject();
			newInst.transform.position = _position;
			newInst.transform.rotation = _rotation;

			return newInst;
		}
		else {
			Debug.LogException(new System.Exception("Object Pool construction failed. No Pooling Data Center established."));
			return null;
		}
	}

	public static void ReturnObject<K>(K _instance, K _source)
	{
		if(Active) {
			if(_instance is Component == false || _source is Component == false) {
				Debug.LogException(new System.Exception("Object Pool return failed. Objects passed in must derive from 'Component'"));
				return;
			}

			Pool targetPool = data.FindMatchingPool(_source);

			if(targetPool == null) {
				GameObject sourceObject = (_source as Component).gameObject;
				targetPool = data.CreateNewPool(sourceObject);
			}

			GameObject instanceObject = (_instance as Component).gameObject;
			targetPool.ReturnObject(instanceObject);
		}
		else {
			Debug.LogException(new System.Exception("Object Pool return failed. No Pooling Data Center established."));
		}
	}

	public static void ReturnObject(GameObject _instance, GameObject _source)
	{
		if(Active) {
			Pool targetPool = data.FindMatchingPool(_source);

			if(targetPool == null) {
				targetPool = data.CreateNewPool(_source);
			}

			targetPool.ReturnObject(_instance);
		}
		else {
			Debug.LogException(new System.Exception("Object Pool construction failed. No Pooling Data Center established."));
		}
	}
}
