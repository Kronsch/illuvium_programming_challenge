﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pool
{
	List<GameObject> allInstances = new List<GameObject>();

	GameObject currentSource;
	Transform currentPoolParent;

	public Pool(GameObject _source, int _startingAmount = 5)
	{
		currentSource = _source;
		currentPoolParent = new GameObject(_source.name + " Object Pool").transform;
		SceneManager.MoveGameObjectToScene(currentPoolParent.gameObject, SceneManager.GetActiveScene());

		CreateObjects(_startingAmount);
	}

	public GameObject GetObject()
	{
		GameObject retObject = null;

		if(allInstances.Count > 0) {
			for(int i = 0; i < allInstances.Count; i++) {
				GameObject inst = allInstances[i];
				if(inst != null) {
					if(inst.activeSelf) {
						continue;
					}

					retObject = inst;
					break;
				}
				else {
					ClearPool();
				}
			}
		}

		if(retObject == null) {
			retObject = CreateObjects(1);
		}
		
		if(allInstances.Contains(retObject)) {
			allInstances.Remove(retObject);
		}
		PooledObjectSetActive(retObject, true);

		return retObject;
	}

	public void ReturnObject(GameObject _instance)
	{
		if(allInstances.Contains(_instance) == false) {
			allInstances.Add(_instance);

			PoolReturner poolReturner = _instance.GetComponent<PoolReturner>();
			if(poolReturner != null) {
				poolReturner.SetPoolReference(this);
			}
		}

		_instance.transform.SetParent(currentPoolParent);
		PooledObjectSetActive(_instance, false);
	}

	public GameObject Source {
		get {
			return currentSource;
		}
	}

	public int Size {
		get {
			return allInstances.Count;
		}
	}

	public void FillPool(int _count)
	{
		int diff = _count - allInstances.Count;

		if(diff > 0) {
			if(currentSource != null && currentPoolParent != null) {
				CreateObjects(diff);
			}
		}
	}

	public void ClearPool(bool _fullClear = false)
	{
		DestroyUnusedObjects();
		if(currentPoolParent != null) {
			GameObject.Destroy(currentPoolParent.gameObject);
		}
		allInstances.Clear();

		if(_fullClear == false) {
			currentPoolParent = new GameObject(currentSource.name + " Object Pool").transform;
			SceneManager.MoveGameObjectToScene(currentPoolParent.gameObject, SceneManager.GetActiveScene());
		}

	}

	GameObject CreateObjects(int _count)
	{
		int objectsLeft = _count;
		GameObject retObject = null;

		while(objectsLeft > 0) {
			GameObject newObject = CreateObject();
			objectsLeft--;

			if(retObject == null) {
				retObject = newObject;
			}
		}

		return retObject;
	}

	GameObject CreateObject()
	{
		GameObject newObject = GameObject.Instantiate(currentSource, currentPoolParent);
		//newObject.hideFlags = HideFlags.HideInHierarchy;
		newObject.SetActive(false);
		allInstances.Add(newObject);

		PoolReturner poolReturner = newObject.GetComponent<PoolReturner>();
		if(poolReturner != null) {
			poolReturner.SetPoolReference(this);
		}

		return newObject;
	}

	void DestroyUnusedObjects()
	{
		for(int i = allInstances.Count - 1; i >= 0; i--) {
			GameObject inst = allInstances[i];

			if(inst != null) {
				if(inst.activeSelf) {
					continue;
				}
				PoolReturner poolReturner = inst.GetComponent<PoolReturner>();
				if(poolReturner != null) {
					poolReturner.ClearPoolReference();
				}

				GameObject.Destroy(inst);
			}

			allInstances.RemoveAt(i);
		}
	}

	void PooledObjectSetActive(GameObject _poolObject, bool _active)
	{
		if(_active == false) {
			//_poolObject.transform.SetParent(currentPoolParent);
		}

		foreach(IPoolingBehaviour inst in _poolObject.GetComponents<IPoolingBehaviour>()) {
			inst.OnPoolingSetActive(_active);
		}

		_poolObject.SetActive(_active);
	}

	public static System.Predicate<Pool> MatchingSource(GameObject _sourceObject)
	{
		return delegate (Pool _p) { return ReferenceEquals(_sourceObject, _p.Source); };
	}

	public static System.Predicate<Pool> MatchingSource<K>(K _sourceObject)
	{
		if(_sourceObject is Component) {
			GameObject sourceObject = (_sourceObject as Component).gameObject;
			return MatchingSource(sourceObject);
		}

		return delegate (Pool _p) { return false; };
	}
}
