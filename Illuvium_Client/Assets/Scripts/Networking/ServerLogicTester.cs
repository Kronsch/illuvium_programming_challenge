using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerLogicTester : MonoBehaviour
{
	[Header("Grid Settings")]
	[SerializeField] BattleSimulator simulator;
	[SerializeField] BattleGridSettings gridSettings;

	[Header("Testing Variables")]
	[SerializeField] List<TeamInfo> teams = new List<TeamInfo>();
	[SerializeField] Vector2Int defaultHPRange;
	[SerializeField] int defaultMoveDist = 14;
	[SerializeField] int defaultAttackRate = 2;
	[SerializeField] int defaultAttackRange = 28;
	
	[SerializeField] bool useAutoSteps = true;
	[SerializeField] float autoServerStepInterval = 1f;

	[SerializeField] KeyCode resetKey;
	[SerializeField] KeyCode forceServerStepKey;

	float autoServerStepTimeLeft;

	AStarGrid pathfindingGrid;

	[Header("State Debugging")]
	[SerializeField] GameStatePrediction currentState;

	private void Start()
	{
		simulator.InitializeSim(gridSettings, teams);
		InitilizePathfinding();

		SetUpDummyServerState();

		simulator.InitialBattleSetup(currentState.battleState);
	}

	private void Update()
    {
		if(autoServerStepTimeLeft <= 0f) {
			SimulateServerStep();

			autoServerStepTimeLeft = autoServerStepInterval;
		}
		else {
			autoServerStepTimeLeft -= Time.deltaTime;
		}

		if(Input.GetKeyDown(resetKey)) {
			ResetSimulation();
		}

		if(Input.GetKeyDown(forceServerStepKey)) {
			SimulateServerStep();
		}
    }

	void ResetSimulation()
	{
		SetUpDummyServerState();
		simulator.InitialBattleSetup(currentState.battleState);
	}

	void SetUpDummyServerState()
	{
		currentState = new GameStatePrediction();

		BattleSnapshot battle = new BattleSnapshot();
		battle.numberOfTeams = teams.Count;

		List<int> teamIDs = new List<int>();
		foreach(TeamInfo team in teams) {
			teamIDs.Add(team.id);
		}
		battle.teamIDs = teamIDs.ToArray();

		List<UnitSnapshot> startingUnits = new List<UnitSnapshot>();
		int currentArenaID = 0;

		foreach(TeamInfo team in teams) {
			int unitsLeft = team.startingUnits;

			while(unitsLeft > 0) {
				UnitSnapshot newUnit = new UnitSnapshot();
				newUnit.unitArenaID = currentArenaID++;

				newUnit.team = team.id;
				newUnit.xCoord = Random.Range(0, gridSettings.GridWidth);
				newUnit.yCoord = Random.Range(0, gridSettings.GridLength);

				newUnit.maxHP = Random.Range(defaultHPRange.x, defaultHPRange.y);
				newUnit.hp = newUnit.maxHP;

				newUnit.moveDistancePerStep = defaultMoveDist;
				newUnit.canMove = true;

				newUnit.hasAttacked = false;
				newUnit.attackCooldownLeft = 0;
				newUnit.attackCooldownSteps = defaultAttackRate;
				newUnit.attackRange = defaultAttackRange;

				newUnit.targetID = -1;

				startingUnits.Add(newUnit);

				unitsLeft--;
			}
		}
		battle.unitState = startingUnits.ToArray();

		currentState.battleState = battle;
		currentState.tickID = 0;

	}

	void InitilizePathfinding()
	{
		pathfindingGrid = new AStarGrid();
		pathfindingGrid.InitializeGrid(gridSettings.GridWidth, gridSettings.GridLength);
	}

	void SimulateServerStep()
	{
		GameStatePrediction newState = new GameStatePrediction();

		NetworkPrediction.PredictGameState(ref newState, currentState, pathfindingGrid);

		simulator.UpdateBattleSnapshot(newState.battleState);

		currentState = newState;
	}
}
