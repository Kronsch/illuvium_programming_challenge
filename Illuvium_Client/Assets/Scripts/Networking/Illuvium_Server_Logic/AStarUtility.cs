using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AStarUtility
{


	public static bool CalculatePath(AStarGrid _grid, out GridCoordinates[] _outputPath, GridCoordinates _start, GridCoordinates _end)
	{
		AStarNode startNode = _grid.GetNode(_start);
		AStarNode endNode = _grid.GetNode(_end);

		List<AStarNode> openSet = new List<AStarNode>();
		HashSet<AStarNode> closedSet = new HashSet<AStarNode>();
		openSet.Add(startNode);

		List<AStarNode> pathResult = null;

		while(openSet.Count > 0) {
			AStarNode currentNode = openSet[0];
			for(int i = 1; i < openSet.Count; i++) {
				if(openSet[i].f_Cost < currentNode.f_Cost || 
					(openSet[i].f_Cost == currentNode.f_Cost && openSet[i].h_Cost < currentNode.h_Cost)) {
					currentNode = openSet[i];
				}
			}

			openSet.Remove(currentNode);
			closedSet.Add(currentNode);


			if(currentNode == endNode) {
				pathResult = RetracePath(startNode, endNode);
				break;
			}

			foreach(AStarNode neighbor in _grid.GetNeighbors(currentNode)) {
				if(closedSet.Contains(neighbor) || !neighbor.walkable) {
					continue;
				}

				int newCostToNeighbor = currentNode.g_Cost + GetDistance(currentNode, neighbor);

				if(newCostToNeighbor < neighbor.g_Cost || !openSet.Contains(neighbor)) {
					neighbor.g_Cost = newCostToNeighbor;
					neighbor.h_Cost = GetDistance(neighbor, endNode);
					neighbor.parent = currentNode;

					if(!openSet.Contains(neighbor)) {
						openSet.Add(neighbor);
					}
				}
			}
		}

		if(pathResult != null) {
			_outputPath = new GridCoordinates[pathResult.Count];
			for(int i = 0; i < pathResult.Count; i++) {
				AStarNode node = pathResult[i];

				_outputPath[i] = new GridCoordinates(node.xCoord, node.yCoord);
			}

			return true;
		}

		_outputPath = new GridCoordinates[0];
		return false;
	}

	private static List<AStarNode> RetracePath(AStarNode _startNode, AStarNode _endNode)
	{
		List<AStarNode> path = new List<AStarNode>();
		AStarNode currentNode = _endNode;

		while(currentNode != _startNode) {
			path.Add(currentNode);
			currentNode = currentNode.parent;
		}
		path.Reverse();

		return path;
	}

	public static int GetDistance(AStarNode _start, AStarNode  _end)
	{
		int xDist = Mathf.Abs(_start.xCoord - _end.xCoord);
		int yDist = Mathf.Abs(_start.yCoord - _end.yCoord);

		return GridCoordinates.TileDistanceCalc(xDist, yDist);
	}
}

public class AStarGrid
{
	AStarNode[,] grid;
	int gridSizeX;
	int gridSizeY;

	public void InitializeGrid(int _width, int _length)
	{
		grid = new AStarNode[_width, _length];

		for(int x = 0; x < _width; x++) {
			for(int y = 0; y < _length; y++) {
				GridCoordinates coords = new GridCoordinates(x, y);
				AStarNode newNode = new AStarNode(coords, true);

				grid[x, y] = newNode;
			}
		}

		gridSizeX = _width;
		gridSizeY = _length;
	}

	public void FlushPathfindingData()
	{
		foreach(AStarNode node in grid) {
			node.g_Cost = 0;
			node.h_Cost = 0;
		}
	}

	public AStarNode GetNode(GridCoordinates _coords)
	{
		int y = _coords.Y;
		if(y < 0 || y >= grid.GetLength(1)) {
			return null;
		}

		int x = _coords.X;
		if(x < 0 || x >= grid.GetLength(0)) {
			return null;
		}

		return grid[x, y];

	}

	public List<AStarNode> GetNeighbors(AStarNode _node)
	{
		List<AStarNode> neighbors = new List<AStarNode>();

		for(int x = -1; x <= 1; x++) {
			for(int y = -1; y <= 1; y++) {
				if(x == 0 && y == 0)
					continue;

				int checkX = _node.xCoord + x;
				int checkY = _node.yCoord + y;

				if(checkX >= 0 && checkX < gridSizeX && checkY >= 0 && checkY < gridSizeY) {
					neighbors.Add(grid[checkX, checkY]);
				}
			}
		}

		return neighbors;
	}
}


public class AStarNode
{
	public int xCoord;
	public int yCoord;

	public bool walkable;

	public int g_Cost;
	public int h_Cost;

	public AStarNode parent;

	public AStarNode(GridCoordinates _coords, bool _walkable)
	{
		xCoord = _coords.X;
		yCoord = _coords.Y;

		walkable = _walkable;
	}

	public int f_Cost {
		get {
			return g_Cost + h_Cost;
		}
	}
}