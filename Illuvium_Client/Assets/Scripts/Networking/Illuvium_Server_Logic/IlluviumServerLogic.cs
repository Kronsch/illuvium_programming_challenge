using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IlluviumServerLogic
{
	private static long SERVER_SIDE_SEED;

	public static void UpdateServerSeed(long _seed)
	{
		SERVER_SIDE_SEED = _seed;
	}

	public static void HandleMovementUpdate(UnitSnapshot[] _units, AStarGrid _grid)
	{
		List<UnitSnapshot> units = new List<UnitSnapshot>(_units);

		for(int i = 0; i < units.Count; i++) {
			UnitSnapshot movingUnit = units[i];

			if(movingUnit.IsDead) {
				continue;
			}

			if(movingUnit.targetID != -1) {
				UnitSnapshot targetUnit = units.Find(delegate (UnitSnapshot _u) { return _u.unitArenaID == movingUnit.targetID; });

				if(targetUnit != null) {
					if(movingUnit.canMove) {
						IndividualMovementStep(movingUnit, targetUnit.GridCoords, _grid);
					}
				}
			}
		}

	}

	public static void IndividualMovementStep(UnitSnapshot _movingUnit, GridCoordinates _targetCoords, AStarGrid _grid)
	{
		GridCoordinates[] path;

		GridCoordinates startTile = _movingUnit.GridCoords;
		GridCoordinates endTile = _targetCoords;

		int maxDistance = _movingUnit.moveDistancePerStep;

		if(AStarUtility.CalculatePath(_grid, out path, startTile, endTile)) {
			GridCoordinates furthestPossibleTile = startTile;
			int furthestTileDistance = 0;

			for(int i = 0; i < path.Length; i++) {
				int distance = path[i].DistanceTo(startTile);

				if(distance > maxDistance) {
					break;
				}

				if(distance > furthestTileDistance) {
					furthestPossibleTile = path[i];
					furthestTileDistance = distance;
				}
			}

			_movingUnit.UpdatePosition(furthestPossibleTile);
		}
	}

	public static void HandleTargetUpdate(UnitSnapshot[] _units)
	{
		Dictionary<int, List<UnitSnapshot>> unitsByTeam = new Dictionary<int, List<UnitSnapshot>>();

		for(int unitIndex = 0; unitIndex < _units.Length; unitIndex++) {
			UnitSnapshot u = _units[unitIndex];
			int teamID = u.team;
			
			if(!unitsByTeam.ContainsKey(teamID)) {
				unitsByTeam.Add(teamID, new List<UnitSnapshot>());
			}

			unitsByTeam[teamID].Add(u);
		}

		for(int unitIndex = 0; unitIndex < _units.Length; unitIndex++) {
			UnitSnapshot u = _units[unitIndex];

			if(u.IsDead) {
				continue;
			}

			IndividualTargetUpdate(u, unitsByTeam);
		}
	}

	public static void IndividualTargetUpdate(UnitSnapshot _unit, Dictionary<int, List<UnitSnapshot>> _allUnits)
	{
		List<UnitSnapshot> potentialTargets = new List<UnitSnapshot>();

		foreach(int teamID in _allUnits.Keys) {
			if(teamID == _unit.team) { 
				continue;
			}
			potentialTargets.AddRange(_allUnits[teamID]);
		}

		GridCoordinates userCoords = new GridCoordinates(_unit.xCoord, _unit.yCoord);

		UnitSnapshot closestTarget = null;
		int closestTargetDistance = int.MaxValue;
		for(int i = 0; i < potentialTargets.Count; i++) {
			if(potentialTargets[i].IsDead) {
				continue;
			}

			int distance = potentialTargets[i].GetDistance(_unit);

			if(distance < closestTargetDistance) {
				closestTarget = potentialTargets[i];
				closestTargetDistance = distance;
			}
		}

		if(closestTarget != null) {
			_unit.targetID = closestTarget.unitArenaID;
		}
		else {
			_unit.targetID = -1;
		}
	}

	public static void HandleAttackUpdate(UnitSnapshot[] _units)
	{
		List<UnitSnapshot> units = new List<UnitSnapshot>(_units);

		for(int i = 0; i < units.Count; i++) {
			UnitSnapshot attackingUnit = units[i];
			bool hasAttacked = false;

			if(attackingUnit.IsDead) {
				attackingUnit.hasAttacked = false;
				continue;
			}

			if(attackingUnit.attackCooldownLeft == 0) {

				if(attackingUnit.targetID != -1) {
					UnitSnapshot targetUnit = units.Find(delegate (UnitSnapshot _u) { return _u.unitArenaID == attackingUnit.targetID; });

					if(targetUnit != null) {
						if(targetUnit.IsDead) {
							attackingUnit.targetID = -1;
							continue;
						}
	
						int distance = attackingUnit.GetDistance(targetUnit);
						if(attackingUnit.attackRange >= distance) {
							targetUnit.InflictDamage(1);
							attackingUnit.attackCooldownLeft = attackingUnit.attackCooldownSteps;

							hasAttacked = true;
						}
					}
				}
			}
			else {
				attackingUnit.attackCooldownLeft--;
			}

			attackingUnit.hasAttacked = hasAttacked;
		
		}
	}

}
