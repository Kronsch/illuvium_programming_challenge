using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class UnitSnapshot
{
	public int unitArenaID;
	public int team;

	public int xCoord;
	public int yCoord;

	public int hp;
	public int maxHP;

	public bool canMove;
	public int moveDistancePerStep;

	public bool hasAttacked;
	public int attackCooldownLeft;
	public int attackCooldownSteps;
	public int attackRange;

	public int targetID;



	public int GetDistance(UnitSnapshot _other)
	{
		GridCoordinates aCoords = new GridCoordinates(xCoord, yCoord);
		GridCoordinates bCoords = new GridCoordinates(_other.xCoord, _other.yCoord);

		return aCoords.DistanceTo(bCoords);
	}

	public void UpdatePosition(GridCoordinates _coords)
	{
		xCoord = _coords.X;
		yCoord = _coords.Y;
	}

	public void InflictDamage(int _dmg)
	{
		hp -= _dmg;
	}

	public bool IsDead {
		get {
			return hp <= 0;
		}
	}

	public GridCoordinates GridCoords {
		get {
			return new GridCoordinates(xCoord, yCoord);
		}
	}
}
