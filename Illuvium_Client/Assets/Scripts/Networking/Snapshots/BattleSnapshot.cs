using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BattleSnapshot
{
	public int numberOfTeams;
	public int[] teamIDs;

	public UnitSnapshot[] unitState;
}
