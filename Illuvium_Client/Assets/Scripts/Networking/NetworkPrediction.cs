using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkPrediction
{
    
	public static void PredictGameState(ref GameStatePrediction _output, GameStatePrediction _current, AStarGrid _pathingGrid)
	{

		BattleSnapshot battle = _current.battleState;
		UnitSnapshot[] unitState = battle.unitState;

		IlluviumServerLogic.HandleTargetUpdate(unitState);
		IlluviumServerLogic.HandleMovementUpdate(unitState, _pathingGrid);
		IlluviumServerLogic.HandleAttackUpdate(unitState);

		battle.unitState = unitState;

		_output.battleState = battle;
		_output.tickID = _current.tickID + 1;

	
	}
}

[System.Serializable]
public class GameStatePrediction
{
	public int tickID;
	public BattleSnapshot battleState;
}
