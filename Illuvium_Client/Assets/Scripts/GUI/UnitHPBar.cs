using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitHPBar : MonoBehaviour
{
	[SerializeField] Image fillImage;
	[SerializeField] float smoothTime = 0.1f;

	float currentFill;
	float targetFill;
	float fillVel;

	private void Update()
	{
		if(currentFill != targetFill) {
			currentFill = fillImage.fillAmount;
			currentFill = Mathf.SmoothDamp(currentFill, targetFill, ref fillVel, smoothTime);

			if(Mathf.Abs(currentFill - targetFill) < Mathf.Epsilon) {
				currentFill = targetFill;
			}

			UpdateFillAmount(currentFill);
		}
	}

	private void UpdateFillAmount(float _normalizedFill)
	{
		fillImage.fillAmount = _normalizedFill;
	}

	public void SetFill(float _normalizedFill, bool _snapToValue = false)
	{
		if(_snapToValue) {
			UpdateFillAmount(_normalizedFill);

			currentFill = _normalizedFill;
			targetFill = _normalizedFill;
			fillVel = 0f;
		}
		else {
			targetFill = _normalizedFill;
		}
	}


}
