using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitCanvas : MonoBehaviour
{
	Canvas canvas;
	Camera mainCamera;

	[SerializeField] Transform container;
	[SerializeField] UnitHPBar hpBar;
	[SerializeField] float headHeight = 1f;

	Unit userRef;

	void Start()
	{
		canvas = GetComponent<Canvas>();
		mainCamera = Camera.main;

		canvas.renderMode = RenderMode.ScreenSpaceOverlay;
		canvas.worldCamera = mainCamera;
	}

	void Update()
	{
		if(userRef != null) {
			if(canvas.enabled) {

				container.position = mainCamera.WorldToScreenPoint(userRef.WorldPos + (Vector3.up * headHeight));

				float sqrDistance = (userRef.WorldPos - mainCamera.transform.position).sqrMagnitude;
				float cameraZoom = sqrDistance / 9000f;
				cameraZoom = Mathf.Clamp01(cameraZoom);
				float scaleMultiplier = FalloffGenerator.Evaluate(1f - cameraZoom, 1f, 1f) * 2f;

				container.localScale = Vector3.one * (1f + scaleMultiplier);
			}
		}
	}

	public void InitializeUnit(Unit _unitRef)
	{
		if(userRef != null) {
			userRef.onHPChanged -= OnUnitHPChanged;
		}

		userRef = _unitRef;

		if(userRef != null) {
			userRef.onHPChanged += OnUnitHPChanged;

			if(hpBar != null) {
				hpBar.SetFill(userRef.NormalizedHealth, true);
			}
		}
	}

	void OnUnitHPChanged()
	{
		if(userRef != null) {
			if(hpBar != null) {
				hpBar.SetFill(userRef.NormalizedHealth);
			}
		}
	
	}
}
