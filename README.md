# illuvium_programming_challenge

Unity version 2020.2.6f1

Helpful Commands:
* R: Resets the simulation during Runtime.
* Space Bar: Forces the simulation to advance one step.

This project demonstrates a Client Prediction Model to simulate an Auto-Battler game. By default, two teams (Red vs. Blue) will face off with a variable number of Units. This all takes place on a 32x32 grid. Units will move toward the closest opponent using A* pathing, and attack until their HP hits zero. Then they will continue to fight until there are no more enemies.

There are two sides to this project: The Battle Simulation, and the Server Simulation. The Server Simulation is where the actual game takes place, or at least, the data driven aspects of it. Important data such as tile position, HP, and whether or not the unit has attacked, is handled on the server side of things. This data is packaged up into a "Snapshot" and sent to the Battle Simulator.

The Battle Simulator, acts almost like a renderer, and simply provides the appropriate visuals to accomodate the data being sent from the server. It's basically a stage with actors, and only acts to match the changes being sent from the IlluviumServerLogic, but does so in a way that appears smooth and continuous.

Important Scripts:
* ServerLogicTester.cs : This is the component used to run the test scene. It contains settings to tweak the simulation from the inspector; like the auto-step rate, starting HP, starting teams, HP range, attack range, move distance, and so on. This component also has a field for viewing the current game state snapshot in the inspector, but it's not very pretty (lots of collapsible fields.)

* NetworkPrediction.cs : This class contains the PredictGameState() method, which takes in the current game state, and outputs what the following game state would be, using the IlluviumServerLogic. The main legwork of this method, takes place in IlluviumServerLogic. Theoretically, this is where the client would call for it's prediction to simulate realtime updates.

* IlluviumServerLogic.cs : Contains all the logic for processing various chunks of the game state, and updating them to reflect things like: Unit A attacks Unit B, or Unit A moves toward Unit B. This is all written on the client side, but in an ideal world, this would perfectly reflect what is happening on the server side by having both utilize the same code. An unused, but included functionality of this script, is to set a seed value called "SERVER_SIDE_SEED" to provide consistent RNG predictions. However, there are no RNG dependent elements in this project, so it's just for demonstration.

